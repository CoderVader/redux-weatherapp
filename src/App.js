import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchWeather } from "./redux/actions/FetchWeather";
import Titles from './components/Titles';
import "./App.css";

function App() {
  const [city, setCity] = useState("");
  // const [ country, setCountry ] = useState("");

  const weatherSelector = useSelector(state => state);
  const dispatch = useDispatch();
  const getWeatherAction = city => dispatch(fetchWeather(city));

  // useEffect(() => {
  //   getWeatherAction("Chennai")
  // }, {})



  const getWeatherInfo = e => {
    e.preventDefault();
    if (city === "") {
      alert("Come on, Enter something! ");
    } else {
      getWeatherAction(city);
      console.log(weatherSelector.weatherInfo);
    }
  };
  let details = "";
  if (weatherSelector.weatherInfo) {
    details = (
      <div className="weather__info">
        <p className="weather__key">Location : 
          <span className="weather__value">
            {weatherSelector.weatherInfo.location.name}, {weatherSelector.weatherInfo.location.country}
          </span>
        </p>

        <p className="weather__key">Temperature : 
          <span className="weather__value">
            {weatherSelector.weatherInfo.current.temp_c} C
          </span>
        </p>

        <p className="weather__key">Humidity : 
          <span className="weather__value">
            {weatherSelector.weatherInfo.current.humidity} g/cub.m
          </span>  
        </p>
        <p className="weather__key"> Description : 
          <span className="weather__value">
            { weatherSelector.weatherInfo.current.condition.text}
          </span> 
          <img src={weatherSelector.weatherInfo.current.condition.icon} alt="Weather Icon"/>
        </p>
        
      </div>
    );
  } else {
    details = "";
  }

  return (
    <div>
      <div className="wrapper">
        <div className="main">
          <div className="container">
            <div className="row">
              <div className="col-xs-5 title-container">
                <Titles />
              </div>

              <div className="col-xs-5 form-container">
              <form onSubmit={getWeatherInfo}>
          
                  <input
                    type="text"
                    name="city"
                    className="input"
                    placeholder="City ... Ex. Paris"
                    onChange={e => setCity(e.target.value)}
                  />
                  <button className="btn btn-outline-success">Get Weather</button>

              </form>
                {details}
              </div>
            </div> {/*row*/}
          </div> {/*container*/}
        </div> {/*main*/}
      </div> {/*wrapper*/}

    </div>
    
  );
}
export default App;
