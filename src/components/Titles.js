import React from 'react';


function Titles() {
    return(
        <div>
            <h1 className="title-container__title">WEATHER NOW</h1>
            <h4 className="title-container__subtitle"> 
                Powered By React | Redux
            </h4>
        </div>
    );
}

export default Titles;