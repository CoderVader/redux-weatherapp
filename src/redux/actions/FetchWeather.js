export function fetchWeather(city) {

  // const API_KEY = "0ada72eb58574da594385139192506";
  return function(dispatch) {
    fetch(
      `https://api.apixu.com/v1/current.json?key=0ada72eb58574da594385139192506&q=${city}`
    ).then(res => {        //The incoming response is converted to a JSON object
        return res.json();
    }).then(JSONres => {       //The received JSON object is 
        dispatch({type: "FETCH_WEATHER", payload: JSONres});
    }).catch(error => {
        console.log(error);
    })
  };

}

// `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`