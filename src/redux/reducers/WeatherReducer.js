const defaultState = {}

export const weatherInfo = (state= defaultState, action) => {
    if(action.type === "FETCH_WEATHER"){
        return {
            ...state,
            weatherInfo:action.payload
        }
    }
    return state;
}

export default weatherInfo;