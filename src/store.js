 import { createStore, applyMiddleware } from 'redux';
 import thunk from 'redux-thunk';
 import reducers from './redux/reducers/WeatherReducer';

 //Middleware
 const middleWare = applyMiddleware(thunk);
 // ---store
 const store = createStore(reducers, middleWare);
 export default store;